package jp.alhinc.nakahara_yu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//コマンドライン引数が適正かどうか
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		if(!inputFile(args[0],"branch.lst",branchNames,branchSales)){
 			return;
		}
 		File[] files = new File(args[0]).listFiles();

 		List<File> rcdFiles = new ArrayList<>();
 		for(int i =0;i < files.length ; i++) {
 			//ファイル名取得
 			String fileName = files[i].getName();
 			if(fileName.matches("^[0-9]{8}.rcd$")) {
 				rcdFiles.add(files[i]);
 				//System.out.println(rcdFiles.get(i));
 			}
 		}
 		for(int i =0;i< rcdFiles.size()-1 ; i++) {
			String saleFile = (rcdFiles.get(i)).getName();
			//rcdFilesにある「00000001.rcd」のファイルからsubstringを使い切り離しを行う
			String saleNumber = saleFile.substring(0,8);
			int fileNumber = Integer.parseInt(saleNumber);
			//System.out.println(fileNumber);

			//比較を行うための値を作成する
			String comparisonFile = (rcdFiles.get(i + 1)).getName();
			String comparisonName = comparisonFile.substring(0,8);
			int comparisonNumber = Integer.parseInt(comparisonName);
	 		//System.out.println(chekNumber);

	 		if(comparisonNumber - fileNumber != 1) {
	 			System.out.println("売り上げファイルが連番になっていない");
	 			return;
	 		}
	 	}
		for(int i =0;i< rcdFiles.size();i++){
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				//支店コードごとの売り上げ金額を表示する
				ArrayList<String> rcdFileDate = new ArrayList<>();
				String line = "";
				while((line = br.readLine()) != null){
					rcdFileDate.add(line);
					System.out.println(line);
				}
				//売上ファイルの売上金額が数字以外のものではないか
				if(!rcdFileDate.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

						//売上ファイルの中身が2行かどうか
				if(rcdFileDate.size() != 2) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です");
					return;
				}

				String fileCode = rcdFileDate.get(0);
				Long fileSales = Long.parseLong(rcdFileDate.get(1));

				//売り上げファイルの支店コードが支店定義ファイルの支店コードに該当するか
				String branchNumber = rcdFileDate.get(0);
				if(!(branchNames.containsKey(branchNumber))){
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です");
					return;
				}
				Long sumSales = (fileSales + branchSales.get(fileCode));
				branchSales.put(fileCode , sumSales);
				System.out.println(branchSales.get(fileCode));
				//集計した売上金額が10桁を超えていないか
				if(sumSales >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
			catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					}
					catch(IOException e) {
						System.out.println("closeできませんでした。");
					return;
					}
				}
			}
		}

 		//if(!branchCheck(rcdFiles,branchNames,branchSales)){
 			//return;}

 		if(!outputFile(args[0],"branch.out",branchNames,branchSales)){
 			return;
 		}
	}
	
	public static boolean inputFile(String path,String branchListFile, Map<String, String>names, Map<String, Long> sales) {
		//支店定義ファイル読み込み処理
		BufferedReader br = null;
		try {
			File file = new File(path, branchListFile);
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				//System.out.println(line);
				String[] items = line.split(",");
				if(items.length != 2 ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//支店コードのフォーマットを正規表現を使ってチェックする
				if(!items[0].matches("^[0-9]{3}$"))  {
					System.out.println("支店コードのフォーマットが不正です");
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		}
		catch(IOException e) {
			System.out.println("エラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("closeできませんでした。");
					return false;
	 			}
	 		}
	 	}
		return true;
	}



	public static boolean outputFile(String path,String fileName,Map<String,String>names,Map<String,Long>sales) {
		//支店ごとの売り上げ金額をファイルに出力する
		BufferedWriter bw = null;
		try {
			File sumFile = new File(path,fileName);
			FileWriter fw = new FileWriter(sumFile);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

